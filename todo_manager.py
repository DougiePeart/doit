import os
import re
from datetime import datetime
from config_manager import load_config
from file_operations import read_todos_from_file, write_todos_to_file

config = load_config()


def expand_path(path):
    expanded_path = os.path.expanduser(os.path.expandvars(path))
#    print(f"Expanding path: {path} to {expanded_path}")  # Debug output
    return expanded_path


def is_excluded(file, excluded_files):
    """Check if the file is in the excluded list."""
    return any(file.endswith(ex) for ex in excluded_files)


def is_todo_line(line, keywords):
    """ Check if a line is a todo line based on keywords. """
    words = line.split()
    return any(keyword in words for keyword in keywords)


def project_files(line, projects):
    """ Check the files specified in the config. """
    return any(projects in line.split())


def extract_date_from_description(description):
    """ Regex to match both full date and date with time. """
    date_pattern = re.compile(r'<(\d{4}-\d{2}-\d{2}(T\d{4})?)>')
    match = date_pattern.search(description)
    if match:
        date_str = match.group(1)
        if 'T' in date_str:
            return datetime.strptime(date_str, '%Y-%m-%dT%H%M')
        return datetime.strptime(date_str, '%Y-%m-%d')
    return None


def load_todos_from_directory(directory):
    todos = []
    excluded_dirs = [expand_path(d) for d in config['Settings']['excluded_dirs']]
    excluded_files = config['Settings']['excluded_files']
    all_keywords = [keyword for state in config['States'].values() for keyword in state['keywords']]

    for root, dirs, files in os.walk(directory):
        dirs[:] = [d for d in dirs if os.path.join(root, d) not in excluded_dirs]
        for file in files:
            full_path = os.path.join(root, file)
            if not is_excluded(file, excluded_files):
                with open(full_path, 'r', errors='ignore') as f:
                    for line_number, line in enumerate(f):
                        if is_todo_line(line, all_keywords):
                            parts = line.strip().split(' ', 1)
                            if len(parts) > 1:
                                keyword, description = parts[0], parts[1]
                                date = extract_date_from_description(description)
                                todos.append({
                                    'file': full_path,
                                    'line': line_number,
                                    'state': keyword,
                                    'description': description,
                                    'date': date
                                })
    return todos


def load_todos_from_file(file_path):
    todos = []
    all_keywords = [keyword for state in config['States'].values() for keyword in state['keywords']]
    with open(file_path, 'r', errors='ignore') as f:
        for line_number, line in enumerate(f):
            if is_todo_line(line, all_keywords):
                parts = line.strip().split(' ', 1)
                if len(parts) > 1:
                    keyword, description = parts[0], parts[1]
                    date = extract_date_from_description(description)
                    todos.append({
                        'file': file_path,
                        'line': line_number,
                        'state': keyword,
                        'description': description,
                        'date': date
                    })
    return todos


def main():
    todo_directories = config['Settings'].get('todo_dirs')
    todo_file = config['Settings'].get('todo_file')
    todos = []

    if todo_directories:
        for directory in todo_directories:
            expanded_directory = expand_path(directory)
            todos.extend(load_todos_from_directory(expanded_directory))
    elif todo_file:
        expanded_file = expand_path(todo_file[0])  # Assuming it's a list with one element
        todos.extend(load_todos_from_file(expanded_file))

    for todo in todos:
        print(f"{todo['file']}: Line {todo['line']} - {todo['state']} - {todo['description']}")


if __name__ == '__main__':
    main()
