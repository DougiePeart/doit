import click
import os
from datetime import datetime, timedelta
from todo_manager import load_todos_from_directory, config, expand_path
from file_operations import read_todos_from_file, write_todos_to_file


@click.group()
def cli():
    """ Your simple todo application. """
    pass


def get_color_for_state(state):
    for category, details in config['States'].items():
        if state in details['keywords']:
            color = details.get('color', 'white')
            return color
    return 'white'  # Default color if no match is found

def get_date_ranges():
    now = datetime.now()
    today = datetime(now.year, now.month, now.day)
    start_of_today = datetime(now.year, now.month, now.day)
    start_of_tomorrow = start_of_today + timedelta(days=1)
    end_of_today = start_of_tomorrow - timedelta(seconds=1)
    start_of_next_week = start_of_today + timedelta(days=(7 - start_of_today.weekday()))
    end_of_this_month = datetime(now.year, now.month + 1, 1) - timedelta(seconds=1) if now.month < 12 else datetime(now.year + 1, 1, 1) - timedelta(seconds=1)
    
    return start_of_today, start_of_tomorrow, end_of_today, start_of_next_week, end_of_this_month, today

def print_task(todo):
    color = get_color_for_state(todo['state'])
    filename = os.path.basename(todo['file'])
    # Use the ordinal function to format the day part of the date
    day = int(todo['date'].strftime('%d'))  # Get day as integer
    day_with_ordinal = ordinal(day)
    # Format the rest of the date
    day_indicator = todo['date'].strftime(f'%A, {day_with_ordinal} %b')
    click.secho(f"{day_indicator}: {filename}:{todo['line']} - {todo['state']} - {todo['description']}", fg=color)


def ordinal(n):
    return "%d%s" % (n, "th" if 11 <= n <= 13 else {1: "st", 2: "nd", 3: "rd"}.get(n % 10, "th"))


@cli.command()
@click.option('--date', default=None, help="Filter tasks by date, e.g., 'today'.")
def list(date):
    """ List todo items. Optionally, filter by a specific date such as 'today'. """
    if date == 'today':
        target_date = datetime.today()
    elif date:
        try:
            target_date = datetime.strptime(date, '%Y-%m-%d')
        except ValueError:
            click.echo("Date format must be YYYY-MM-DD.")
            return

    todo_directories = config['Settings']['todo_dirs']
    todos = []
    for directory in todo_directories:
        expanded_directory = expand_path(directory)
        todos.extend(load_todos_from_directory(expanded_directory))
    
    if date:
        # Filter todos to include only those matching the target_date
        todos = [todo for todo in todos if todo['date'] and todo['date'].date() == target_date.date()]

    for index, todo in enumerate(todos):
        color = get_color_for_state(todo['state'])
        filename = os.path.basename(todo['file'])
        click.secho(f"{index}: {filename}:{todo['line']} - {todo['state']} - {todo['description']}", fg=color)


@cli.command()
@click.option('--view', default='standard', type=click.Choice(['standard', 'today','tomorrow', 'week', 'month', 'all']))
@click.option('--custom', default=None, help="Specify a custom view defined in the config.")
def agenda(view, custom):
    """ Display tasks in an agenda view. """
    if custom:
        display_custom_agenda(custom)
        return
    today, start_of_today, start_of_tomorrow, end_of_today, start_of_next_week, end_of_this_month = get_date_ranges()

    todo_directories = config['Settings']['todo_dirs']
    todos = []
    for directory in todo_directories:
        expanded_directory = expand_path(directory)
        todos.extend(load_todos_from_directory(expanded_directory))
    filtered_todos = todos

    if view == 'standard':
        print("\nOverdue Tasks:")
        overdue_todos = [todo for todo in todos if todo.get('date') and todo['date'] < today and not todo['state'].lower() == 'done']
        for todo in overdue_todos:
            print_task(todo)

        print("\nToday's Tasks:")
        today_todos = [todo for todo in todos if todo['date'] and start_of_today <= todo['date'] <= end_of_today]
        for todo in today_todos:
            print_task(todo)

        print("\n---\nTomorrow's Tasks:")
        tomorrow_todos = [todo for todo in todos if todo['date'] and start_of_tomorrow <= todo['date'] < start_of_tomorrow + timedelta(days=1)]
        for todo in tomorrow_todos:
            print_task(todo)

        print("\n---\nThis Week's Tasks:")
        week_todos = [todo for todo in todos if todo['date'] and start_of_today <= todo['date'] < start_of_next_week]
        for todo in week_todos:
            print_task(todo)

        print("\n---\nThis Month's Tasks:")
        month_todos = [todo for todo in todos if todo['date'] and start_of_today <= todo['date'] <= end_of_this_month]
        for todo in month_todos:
            print_task(todo)
        pass
    else:
    # Filter and sort todos based on selected view
            if view == 'today':
                filtered_todos = [todo for todo in todos if todo['date'] and todo['date'].date() == start_of_today.date()]
            elif view == 'tomorrow':
                filtered_todos = [todo for todo in todos if todo['date'] and start_of_tomorrow <= todo['date'] < start_of_tomorrow + timedelta(days=1)]
            elif view == 'week':
                filtered_todos = [todo for todo in todos if todo['date'] and start_of_today.date() <= todo['date'].date() < start_of_next_week.date()]
            elif view == 'month':
                filtered_todos = [todo for todo in todos if todo['date'] and start_of_today <= todo['date'] <= end_of_month]
            else:
                filtered_todos = todos  # No filtering for 'all'

    # Sort and display todos
    filtered_todos.sort(key=lambda x: x.get('date') if x.get('date') is not None else datetime.max)

    for todo in filtered_todos:
        color = get_color_for_state(todo['state'])
        filename = os.path.basename(todo['file'])
        day_indicator = todo['date'].strftime('%A, %b %d') if todo['date'] else "Date Not Set"
        click.secho(f"{day_indicator}: {filename}:{todo['line']} - {todo['state']} - {todo['description']}", fg=color)


def display_custom_agenda(custom_view):
    """ Display tasks based on a custom view defined in the config. """
    custom_views = config['Settings'].get('CustomViews', {})
    if custom_view not in custom_views:
        click.echo(f"Custom view '{custom_view}' not found.")
        return

    view_settings = custom_views[custom_view]
    keywords = view_settings.get('keywords', [])
    within_days = view_settings.get('within_days', None)
    exclude_keywords = view_settings.get('exclude_keywords', [])

    todos = []
    for directory in config['Settings']['todo_dirs']:
        expanded_directory = expand_path(directory)
        todos.extend(load_todos_from_directory(expanded_directory))

    now = datetime.now()
    filtered_todos = [
        todo for todo in todos
        if any(kw in todo['description'] for kw in keywords) and
           (not within_days or (todo.get('date') and (todo['date'] - now).days <= within_days)) and
           not any(kw in todo['description'] for kw in exclude_keywords)
    ]

    if not filtered_todos:
        click.echo("No tasks match the custom view criteria.")
    else:
        for todo in filtered_todos:
            print_task(todo)


@cli.command()
@click.argument('index', type=int)
def delete(index):
    """Delete a todo item."""
    todos = []
    for directory in config['Settings']['todo_dirs']:  # Iterate over each directory
        expanded_directory = expand_path(directory)
        todos.extend(load_todos_from_directory(expanded_directory))  # Pass each directory to the function
    if 0 <= index < len(todos):
        todo = todos[index]
        lines = read_todos_from_file(todo['file'])
        lines = [line for i, line in enumerate(lines) if i != todo['line']]
        write_todos_to_file(todo['file'], lines)
        click.echo(f"Deleted todo at index {index}.")
    else:
        click.echo("Invalid index. Please try again")


if __name__ == "__main__":
    cli()
