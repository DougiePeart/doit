from click.testing import CliRunner
from cli import cli


def test_list():
    runner = CliRunner()
    result = runner.invoke(cli, ['list'])
    assert result.exit_code == 0
    assert "No tasks found" in result.output or "1: task1.md:1 - TODO" in result.output


def test_agenda():
    runner = CliRunner()
    result = runner.invoke(cli, ['agenda', '--view', 'today'])
    assert result.exit_code == 0
    assert "Today's Tasks:" in result.output
