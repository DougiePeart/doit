def read_todos_from_file(filename):
    """ Read lines from a file. """
    with open(filename, 'r') as file:
        return file.readlines()


def write_todos_to_file(filename, lines):
    """ Write lines back to the specified file. """
    with open(filename, 'w') as file:
        file.writelines(lines)
