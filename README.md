# What is this?

Simply put Doit is a todo application for tracking your tasks.

![GIF](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZGJ6MHhhZmFlem1oMnh3dmFqeXl6MTVvbWRyNjR2cnpxaTN5czZmYSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/pTQUOfSmjo2hG/giphy.gif)

## Configuration
To set up the configuration for Doit, please copy the example configuration from the examples folder into `$XDG_CONFIG_HOME/doit/` (typically `~/.config/doit/`). 

You can customize several aspects:
- **States**: Define task states like Future, In-progress, Urgent, and Done. Each can have associated keywords and ANSI colors.
- **todo_dirs**: Specify one or more directories to search for todo items in various text formats.
- **todo_file**: Use a single file for todos. Note: Leave `todo_dirs` blank for this setting to take effect.
- **excluded_directories**: Define directories that Doit should ignore during searches.
- **excluded_files**: List specific files to exclude.
- **CustomViews**: Create customized views for the agenda command using the `--custom` flag.

### Available Color Options:
- black
- red
- green
- yellow
- blue
- magenta
- cyan
- white

## Usage

### Basic Commands
#### List Tasks
- `list`: Lists all todo items, optionally filtered by date.
  ```bash
  doit list --date 2024-05-12
  doit list --date today
  ```
#### Delete Tasks
- delete: Removes a task by its index. This command is useful for managing tasks once they are completed or no longer relevant. The index corresponds to the position of the task in the list output.
```bash
  doit delete 3
```

### Agenda View
- View tasks in different time frames like today, this week, or a custom-defined period.
  ```bash
  doit agenda --view week
  doit agenda --custom CriticalTasks
  ```
