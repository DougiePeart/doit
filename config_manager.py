import os
import json


def load_config(path=None):
    """ Load the configuration file. """
    if path is None:
        # Use os.getenv to get environment variable with a fallback
        config_home = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config'))
        path = os.path.join(config_home, 'doit', 'config.json')

    try:
        with open(path, 'r') as file:
            return json.load(file)
    except FileNotFoundError:
        print(f"Configuration file not found at {path}. Please ensure 'config.json' exists.")
        exit(1)
    except json.JSONDecodeError:
        print("Error decoding 'config.json'. Please ensure it's valid JSON.")
        exit(1)
